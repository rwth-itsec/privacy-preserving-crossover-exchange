# coding: latin-1
"""
Implementation of the gate for compatibility check from Malte Breuer, Ulrike Meyer, Susanne Wetzel,
and Anja Muhlfeld. 2020. A Privacy-Preserving Protocol for the Kidney Exchange Problem. In Workshop on Privacy in the
Electronic Society. ACM. Note that this gate was originally implemented based on homomorphic encryption whereas this
implementation is based on secret sharing. """

from Compiler.types import sint, regint, Array, MemValue, Matrix
from Compiler.library import print_ln, do_while, for_range, if_, print_str
from Compiler.util import if_else, or_op
from Compiler.networking import write_output_to_clients, client_input, accept_client

NUM_NODES = 3

# number of bloodtypes and number of antigens for the different loci
BLOOD_TYPES = 4
ANTIGEN_TYPES = 82
ANTIGEN_TYPES_A = 21
ANTIGEN_TYPES_B = 35
ANTIGEN_TYPES_C = 8
ANTIGEN_TYPES_DQ = 5
ANTIGEN_TYPES_DR = 13


def compute_compatibility(blood_donor, antigen_donor, blood_patient, antigen_patient):
    """ Computes the compatibility between one donor and one patient based on bloodtype and HLA antigens/antibodies. """
    sumb = Array(1, sint)
    sumb[0] = sint(0)

    suma = Array(1, sint)
    suma[0] = sint(0)

    @for_range(BLOOD_TYPES)
    def loop_one(i):
        sumb[0] = if_else(blood_patient[i], sumb[0] + blood_donor[i], sumb[0])

    @for_range(ANTIGEN_TYPES)
    def loop_two(j):
        suma[0] = if_else(antigen_patient[j], suma[0] + antigen_donor[j], suma[0])

    ohb = sint(0) < sumb[0]
    oha = suma[0] < sint(1)
    oall = ohb * oha

    return oall


def compute_comp_matrix(number_clients, reveal_matrix):
    """ Computes the adjacency matrix for the directed compatibility graph where each input peer corresponds to one
    node in the graph and an edge is added from node x to node y if the donor of input peer x is compatible with the
    patient of input peer y. """
    blood_donor, blood_patient, antigen_donor, antigen_patient = read_input(number_clients)

    adjacency_matrix = sint.Matrix(NUM_NODES, NUM_NODES)

    if reveal_matrix:
        print_ln("Adjacency Matrix:")
    else:
        print_ln("Computing the adjacency matrix...")

    @for_range(number_clients)
    def _(client_i):
        @for_range(number_clients)
        def _(client_j):
            if reveal_matrix:
                @if_(client_j == client_i)
                def _():
                    print_str("- ")

            @if_(client_j != client_i)
            def _():
                adjacency_matrix[client_i][client_j] = compute_compatibility(blood_donor[client_i],
                                                                             antigen_donor[client_i],
                                                                             blood_patient[client_j],
                                                                             antigen_patient[client_j])

                if reveal_matrix:
                    output = adjacency_matrix[client_i][client_j].reveal()
                    print_str("%s ", output)

        if reveal_matrix:
            print_ln(" ")

    return adjacency_matrix


def read_input(number_clients):
    """ Read in the secret input of all input peers. The input comprises the bloodtype indicator vector and the
    antigen (antibody) indicator vector of patient and donor of each input peer. """

    blood_donor = sint.Matrix(NUM_NODES, BLOOD_TYPES)
    blood_patient = sint.Matrix(NUM_NODES, BLOOD_TYPES)
    antigen_donor = sint.Matrix(NUM_NODES, ANTIGEN_TYPES)
    antigen_patient = sint.Matrix(NUM_NODES, ANTIGEN_TYPES)

    @for_range(number_clients)
    def _(client_id):
        blood_donor[client_id] = client_input(client_id, BLOOD_TYPES)

    @for_range(number_clients)
    def _(client_id):
        blood_patient[client_id] = client_input(client_id, BLOOD_TYPES)

    @for_range(number_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_A)
        for i in range(ANTIGEN_TYPES_A):
            antigen_donor[client_id][i] = tmp[i]

    @for_range(number_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_A)

        for i in range(ANTIGEN_TYPES_A):
            antigen_patient[client_id][i] = tmp[i]

    @for_range(number_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_B)

        for i in range(ANTIGEN_TYPES_B):
            antigen_donor[client_id][ANTIGEN_TYPES_A + i] = tmp[i]

    @for_range(number_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_B)

        for i in range(ANTIGEN_TYPES_B):
            antigen_patient[client_id][ANTIGEN_TYPES_A + i] = tmp[i]

    @for_range(number_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_C)

        for i in range(ANTIGEN_TYPES_C):
            antigen_donor[client_id][ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + i] = tmp[i]

    @for_range(number_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_C)

        for i in range(ANTIGEN_TYPES_C):
            antigen_patient[client_id][ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + i] = tmp[i]

    @for_range(number_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_DQ)

        for i in range(ANTIGEN_TYPES_DQ):
            antigen_donor[client_id][ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + ANTIGEN_TYPES_C + i] = tmp[i]

    @for_range(number_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_DQ)

        for i in range(ANTIGEN_TYPES_DQ):
            antigen_patient[client_id][ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + ANTIGEN_TYPES_C + i] = tmp[i]

    @for_range(number_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_DR)

        for i in range(ANTIGEN_TYPES_DR):
            antigen_donor[client_id][ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + ANTIGEN_TYPES_C + ANTIGEN_TYPES_DQ + i] = \
                tmp[i]

    @for_range(number_clients)
    def _(client_id):
        tmp = client_input(client_id, ANTIGEN_TYPES_DR)

        for i in range(ANTIGEN_TYPES_DR):
            antigen_patient[client_id][ANTIGEN_TYPES_A + ANTIGEN_TYPES_B + ANTIGEN_TYPES_C + ANTIGEN_TYPES_DQ + i] = \
                tmp[i]

    return blood_donor, blood_patient, antigen_donor, antigen_patient
