# coding: latin-1
"""
Implementation of the protocol KEP-Rnd from Malte Breuer, Ulrike Meyer, Susanne Wetzel,
and Anja Muhlfeld. 2020. A Privacy-Preserving Protocol for the Kidney Exchange Problem. In Workshop on Privacy in the
Electronic Society. ACM.
While the protocol in the original paper was based on homomorphic encryption, this implementation uses secret sharing.
"""

from Compiler.types import sint, regint, Array, MemValue, Matrix, MultiArray
from Compiler.library import print_ln, for_range, for_range_opt, for_range_parallel
from Compiler.util import if_else, or_op
from Compiler.library import if_e, else_, if_, public_input
from Compiler.exceptions import CompilerError
from Compiler.permutation import iter_waksman, config_shuffle
from Compiler.comp_gate import compute_comp_matrix
from Compiler.networking import write_prime_to_clients, client_input, accept_client, setup_client_connections
import math

# Set to True for additional output
DEBUG = False

# THESE VARIABLES CAN BE CHANGED TO ALTER THE HOW THE PROTOCOL IS EXECUTED
# number of input peers
NUM_CLIENTS = 3

# maximal length of exchange cycles that shall be considered, 2 or 3
MAX_CYCLE_SIZE = 2

# THESE VARIABLES ONLY IMPACT THE PERFORMANCE
NUM_PARALLEL = 32

# THESE VARIABLES SHOULD NOT BE ALTERED
PORTNUM = 14000
INPUT_LENGTH = 40
BLOOD_TYPES = 4
ANTIGEN_TYPES = 82
ANTIGEN_TYPES_A = 21
ANTIGEN_TYPES_B = 35
ANTIGEN_TYPES_C = 8
ANTIGEN_TYPES_DQ = 5
ANTIGEN_TYPES_DR = 13

# THESE VARIABLES CORRESPOND TO THE SET OF EXCHANGE CONSTELLATION GRAPHS FOR i INPUT PEERS
# For execution with cycles of size <= 2
NUM_GRAPHS_LIST_C2 = [0, 1, 2, 4, 10, 26, 76, 232, 764, 2620, 9496, 35696, 140152, 568504, 2390480,
                      10349536]  # Number of Exchange Constellation Graphs for i input peers
NUM_GRAPHS_LIST_LOGS_C2 = [-1, 0, 1, 2, 4, 5, 7, 8, 10, 12, 14, 16, 18, 20, 22,
                           24]  # logarithms of next biggest power of 2
NUM_GRAPHS_LIST_POW2_C2 = [0, 1, 2, 4, 16, 32, 128, 256, 1024, 4096, 16384, 65536, 262144, 1048576, 4194304,
                           16777216]  # padded to power of 2 for Waksman network

# For execution with cycles of size <= 3
NUM_GRAPHS_LIST_C3 = [0, 1, 2, 6, 18, 66, 276, 1212, 5916, 31068, 171576,
                      1014696]  # Number of Exchange Constellation for i input peers
NUM_GRAPHS_LIST_LOGS_C3 = [-1, 0, 1, 3, 5, 7, 9, 11, 13, 15, 18, 20]  # logarithms of next biggest power of 2
NUM_GRAPHS_LIST_POW2_C3 = [0, 1, 2, 8, 32, 128, 512, 2048, 8192, 32768, 262144,
                           1048576]  # padded to power of 2 for Waksman network

# Creates variables and lists depending on the number of clients and on the maximum cycle length
if MAX_CYCLE_SIZE == 2:
    NUM_GRAPHS = NUM_GRAPHS_LIST_C2[NUM_CLIENTS]
    NUM_GRAPHS_POW2 = NUM_GRAPHS_LIST_POW2_C2[NUM_CLIENTS]
    NUM_GRAPHS_LOG = NUM_GRAPHS_LIST_LOGS_C2[NUM_CLIENTS]
    NUM_GRAPHS_ARRAY = Array.create_from([regint(x) for x in NUM_GRAPHS_LIST_C2])

elif MAX_CYCLE_SIZE == 3:
    NUM_GRAPHS = NUM_GRAPHS_LIST_C3[NUM_CLIENTS]
    NUM_GRAPHS_POW2 = NUM_GRAPHS_LIST_POW2_C3[NUM_CLIENTS]
    NUM_GRAPHS_LOG = NUM_GRAPHS_LIST_LOGS_C3[NUM_CLIENTS]
    NUM_GRAPHS_ARRAY = Array.create_from([regint(x) for x in NUM_GRAPHS_LIST_C3])

# Graph length corresponds to the input length of one graph, i.e.,
# the number of integers in a line of the public input file
GRAPH_LENGTH = 2 * NUM_CLIENTS

if len(program.args) > 1:
    n_rounds = int(program.args[1])


def shuffle_once(x, config=None, value_type=sint, reverse=False):
    """ Simulate secure shuffling with Waksman network for 3 players.
    Has to be called once with config from C1 and once with config from C2
    Returns the network switching config so it may be re-used later.  """
    n = len(x)
    m = 2 ** int(math.ceil(math.log(n, 2)))
    assert n == m, 'only working for powers of two'
    if config is None:
        config = config_shuffle(n, value_type)

    if isinstance(x, list):
        if isinstance(x[0], list):
            length = len(x[0])
            assert len(x) == length
            for i in range(length):
                xi = Array(m, value_type.reg_type)
                for j in range(n):
                    xi[j] = x[j][i]
                for j in range(n, m):
                    xi[j] = value_type(0)
                iter_waksman(xi, config, reverse=reverse)
                for j, y in enumerate(xi):
                    x[j][i] = y
        else:
            xa = Array(m, value_type.reg_type)
            for i in range(n):
                xa[i] = x[i]
            for i in range(n, m):
                xa[i] = value_type(0)
            iter_waksman(xa, config, reverse=reverse)
            x[:] = xa
    elif isinstance(x, Array):
        if len(x) != m and config is None:
            raise CompilerError('Non-power of 2 Array input not yet supported')
        iter_waksman(x, config, reverse=reverse)
    else:
        raise CompilerError('Invalid type for shuffle:', type(x))

    return config


def compute_matching(ec_permutations, adjacency_matrix, client_primes, config_cp0, config_cp1):
    # EVALUATION PHASE

    # Secret shared bit indicating for each graph if it is a potential exchange constellation graph
    exchange_possible = Array(NUM_GRAPHS_POW2, sint)
    exchange_possible.assign_all(sint(1))

    # Matrix for parallelization of 2-cycles
    # Stores for each client a list of (Graph ID, Exchange Partner) tuples
    # Each cycles for each graph will be added exactly once to the matrix (for the first client in the cycle)
    two_cycle_instructions = MultiArray([NUM_CLIENTS, NUM_GRAPHS, 2], regint)

    # Array for storing lengths of the (Graph ID, Exchange Partner) lists
    two_cycle_indices = regint.Array(NUM_CLIENTS)
    two_cycle_indices.assign_all(0)

    # The same for 3-cycles:
    # Stores for each client a list of (Graph ID, Exchange Partner 1, Exchange Partner 2) tuples
    three_cycle_instructions = MultiArray([NUM_CLIENTS, NUM_GRAPHS, 3], regint)

    # Array for storing lengths of the (Graph ID, Exchange Partner 1, Exchange Partner 2) lists
    three_cycle_indices = regint.Array(NUM_CLIENTS)
    three_cycle_indices.assign_all(0)

    # No secret shared computation happens in this block
    # The Matrices are set up for parallel computation
    # iterates over all graphs (set G^EC) and updates exchange_possible for each graph
    @for_range(NUM_GRAPHS)  # for each exchange constellation graph
    def graph_index(i):
        current_cycle_length = MemValue(0)  # stores the length of the current cycle

        @for_range(GRAPH_LENGTH)  # iterates over the exchange constellation graph string specified in public input file
        def _(pos):
            @if_e(ec_permutations[i][pos] == 0)  # end of crossover/cycle found, 0 indicates stop
            def _():
                @if_e(current_cycle_length.read() == 2)  # if a crossover exchange was found
                def crossover_exchange():
                    # id of first client in exchange is ec_permutations[i][pos - 2] - 1
                    first_client_id = ec_permutations[i][pos - 2] - 1
                    # Sets graph index
                    two_cycle_instructions[first_client_id][two_cycle_indices[first_client_id]][0] = i
                    # Sets exchange partner
                    two_cycle_instructions[first_client_id][two_cycle_indices[first_client_id]][1] \
                        = ec_permutations[i][pos - 1] - 1
                    # Update list length for the first client in the cycle
                    two_cycle_indices[first_client_id] = two_cycle_indices[first_client_id] + 1

                @else_
                def _():
                    @if_(current_cycle_length.read() == 3)  # if cyclic exchange was found
                    def cyclic_exchange():  # Works similar to the crossover_exchange case
                        first_client_id = ec_permutations[i][pos - 3] - 1
                        three_cycle_instructions[first_client_id][three_cycle_indices[first_client_id]][0] = i
                        three_cycle_instructions[first_client_id][three_cycle_indices[first_client_id]][1] \
                            = ec_permutations[i][pos - 2] - 1
                        three_cycle_instructions[first_client_id][three_cycle_indices[first_client_id]][2] \
                            = ec_permutations[i][pos - 1] - 1
                        three_cycle_indices[first_client_id] = three_cycle_indices[first_client_id] + 1

            current_cycle_length.write(0)  # Prepare computing length of next cycle

            @else_  # if the number that was read is not a 0
            def _():  # current cycle length is updated
                current_cycle_length.write(current_cycle_length.read() + 1)

    # The parallel computation begins here
    # 2-Cycles
    @for_range(NUM_CLIENTS)
    def client(i):
        # iterates over all cycles which start with input peer i
        @for_range_parallel(NUM_PARALLEL, two_cycle_indices[i])
        def cycle(j):
            # update exchange_possible for the considered cycle
            exchange_possible[two_cycle_instructions[i][j][0]] = exchange_possible[two_cycle_instructions[i][j][0]] * \
                                                                 adjacency_matrix[i][two_cycle_instructions[i][j][1]] * \
                                                                 adjacency_matrix[two_cycle_instructions[i][j][1]][i]

    # 3-Cycles
    @for_range(NUM_CLIENTS)
    def client(i):
        # iterates over all cycles which start with input peer i
        @for_range_parallel(NUM_PARALLEL, three_cycle_indices[i])
        def cycle(j):
            # update exchange_possible for the considered cycle
            exchange_possible[three_cycle_instructions[i][j][0]] = exchange_possible[
                                                                       three_cycle_instructions[i][j][0]] * \
                                                                   adjacency_matrix[i][
                                                                       three_cycle_instructions[i][j][1]] * \
                                                                   adjacency_matrix[three_cycle_instructions[i][j][1]][
                                                                       three_cycle_instructions[i][j][2]] * \
                                                                   adjacency_matrix[three_cycle_instructions[i][j][2]][
                                                                       i]

    print_ln("Starting Prioritization Phase")

    # PRIORITIZATION PHASE

    # stores the welfare for each exchange constellaton graph
    # size is power of two for shuffling later
    exchange_welfare = Array(NUM_GRAPHS_POW2, sint)
    exchange_welfare.assign_all(sint(0))

    # this works similar to the evaluation phase
    # in contrast to the evaluation phase, here the computation happens in this block as it is not parallelized
    # since the workload is negligible
    @for_range(NUM_GRAPHS)
    def graph_index(i):
        current_cycle_length = MemValue(0)

        @for_range(GRAPH_LENGTH)
        def _(pos):
            @if_e(ec_permutations[i][pos] == 0)  # end of cycle found
            def _():
                @if_e(current_cycle_length.read() == 2)
                def crossover_exchange():
                    exchange_welfare[i] = exchange_welfare[i] + sint(2)

                @else_
                def _():
                    @if_(current_cycle_length.read() == 3)
                    def cyclic_exchange():
                        exchange_welfare[i] = exchange_welfare[i] + sint(3)

            current_cycle_length.write(0)

            @else_
            def _():  # current cycle length is updated
                current_cycle_length.write(current_cycle_length.read() + 1)

    @for_range_opt(NUM_GRAPHS)
    def _(i):
        exchange_welfare[i] *= exchange_possible[i]

    print_ln("Starting Mapping Phase")

    # MAPPING PHASE

    prime_products = Array(NUM_GRAPHS_POW2, sint)
    prime_products.assign_all(sint(1))

    # This Matrix stores for each client and each exchange constellation graph the two exchange partners of the client
    client_constellations = MultiArray([NUM_CLIENTS, NUM_GRAPHS, 2], regint)

    # similar to the evaluation phase
    @for_range(NUM_GRAPHS)
    def graph_index(i):
        current_cycle_length = MemValue(0)

        @for_range(GRAPH_LENGTH)
        def _(pos):
            @if_e(ec_permutations[i][pos] == 0)  # end of cycle found
            def _():
                @if_e(current_cycle_length.read() == 1)
                def no_exchange():
                    first_client_id = ec_permutations[i][pos - 1] - 1

                    client_constellations[first_client_id][i][0] = ec_permutations[i][pos - 1] - 1
                    client_constellations[first_client_id][i][1] = ec_permutations[i][pos - 1] - 1

                @else_
                def _():
                    @if_e(current_cycle_length.read() == 2)
                    def crossover_exchange():
                        first_client_id = ec_permutations[i][pos - 2] - 1
                        second_client_id = ec_permutations[i][pos - 1] - 1

                        client_constellations[first_client_id][i][0] = second_client_id
                        client_constellations[first_client_id][i][1] = second_client_id

                        client_constellations[second_client_id][i][0] = first_client_id
                        client_constellations[second_client_id][i][1] = first_client_id

                    @else_
                    def _():
                        @if_(current_cycle_length.read() == 3)
                        def cyclic_exchange():
                            first_client_id = ec_permutations[i][pos - 3] - 1
                            second_client_id = ec_permutations[i][pos - 2] - 1
                            third_client_id = ec_permutations[i][pos - 1] - 1

                            client_constellations[first_client_id][i][0] = third_client_id
                            client_constellations[first_client_id][i][1] = second_client_id

                            client_constellations[second_client_id][i][0] = first_client_id
                            client_constellations[second_client_id][i][1] = third_client_id

                            client_constellations[third_client_id][i][0] = second_client_id
                            client_constellations[third_client_id][i][1] = first_client_id

            current_cycle_length.write(0)

            @else_
            def _():  # current cycle length is updated
                current_cycle_length.write(current_cycle_length.read() + 1)

    # Actual multiplication happens here due to parallelization of the secret shared multiplications
    @for_range(NUM_CLIENTS)
    def client(j):
        @for_range_parallel(NUM_PARALLEL, NUM_GRAPHS)
        def graph(i):
            prime_products[i] = prime_products[i] * client_primes[j] \
                [client_constellations[j][i][0]] \
                [client_constellations[j][i][1]]

    print_ln("Starting Selection Phase")

    # First shuffle with Waksman network configuration bits by C1
    # shuffle_once is similar to the insecure shuffle implementation in MP-SPDZ
    # however, shuffle_once is secure as the permutation for shuffling is chosen privately once by C1 and once by C2
    # prior to the joint computation
    shuffle_once(exchange_welfare, config=config_cp0, value_type=sint)
    shuffle_once(prime_products, config=config_cp0, value_type=sint)

    # Second shuffle with Waksman network configuration bits by C2
    shuffle_once(exchange_welfare, config=config_cp1, value_type=sint)
    shuffle_once(prime_products, config=config_cp1, value_type=sint)

    # BUBBLE UP
    distance = Array(1, regint)
    distance_half = Array(1, regint)
    distance[0] = 2
    distance_half[0] = 1

    @for_range(NUM_GRAPHS_LOG)
    def _(iteration):  # bubbles up maximal entry in the subarrays of size 2, then 4, then 8, ...
        @for_range_parallel(NUM_PARALLEL, NUM_GRAPHS_POW2 / distance[0])
        def _(j):
            left_is_larger = exchange_welfare[j * distance[0]] > exchange_welfare[j * distance[0] + distance_half[0]]
            exchange_welfare[j * distance[0]] = if_else(left_is_larger, exchange_welfare[j * distance[0]],
                                                        exchange_welfare[j * distance[0] + distance_half[0]])
            prime_products[j * distance[0]] = if_else(left_is_larger, prime_products[j * distance[0]],
                                                      prime_products[j * distance[0] + distance_half[0]])

        distance[0] = 2 * distance[0]
        distance_half[0] = 2 * distance_half[0]

    # Output Check of Conditional Random Selection gate
    g_sec_prime = if_else(exchange_welfare[0], prime_products[0], 0)

    return g_sec_prime


# receives configuration bits from one computing peer and secret shares the bits among all computing peers
def input_waksman_config_from_player(non_oblivious_player, config):
    @for_range(NUM_GRAPHS_LOG)
    def _(i):
        @for_range(NUM_GRAPHS_POW2)
        def _(j):
            config[i * NUM_GRAPHS_POW2 + j] = sint.get_input_from(non_oblivious_player)


# stores exchange constellation graphs into array ec_graphs from public input
def receive_input_permutations(ec_graphs):
    @for_range(0, ec_graphs.sizes[0])
    def _(i):
        @for_range(0, ec_graphs.sizes[1])
        def _(j):
            ec_graphs[i][j] = public_input()


def main():
    number_clients, client_sockets = setup_client_connections(PORTNUM, NUM_CLIENTS)

    # client_primes is structured like this: [row: receive_kidney_from_client, column: give_kidney_to_client]
    # from: down, to: right
    client_primes = MultiArray([NUM_CLIENTS, NUM_CLIENTS, NUM_CLIENTS], sint)

    # configuration bits for Waksman networks
    config_cp0 = Array(NUM_GRAPHS_LOG * NUM_GRAPHS_POW2, sint)
    config_cp1 = Array(NUM_GRAPHS_LOG * NUM_GRAPHS_POW2, sint)

    # Computing peers 0 and 1 secret share config for Waksman network
    # The Waksman network is applied in the selection phase to shuffle the graphs
    print_ln("Starting Precomputation")
    input_waksman_config_from_player(0, config_cp0)
    input_waksman_config_from_player(1, config_cp1)

    # Generates Multidimensional Matrix which will be filled with EC_Graphs
    # Each EC_Graph is an Array, e.g. [1, 2, 3, 0, 4, 0, -5, 6, 7, 0] encodes the following exchanges:
    # Cyclic Exchange: 1->2, 2->3, 3->1,
    # 4 does not participate in any exchange and
    ec_graphs = MultiArray([NUM_GRAPHS, GRAPH_LENGTH], regint)

    # Reads Exchange Constellation Graphs from public input file
    receive_input_permutations(ec_graphs)

    print_ln("Starting Protocol Execution.")

    # CONSTRUCTION PHASE
    print_ln("Starting Construction Phase")

    adjacency_matrix = compute_comp_matrix(number_clients, DEBUG)

    @for_range(number_clients)
    def _(client_id):
        tmp = client_input(client_id, NUM_CLIENTS * NUM_CLIENTS)

        for i in range(NUM_CLIENTS):
            for j in range(NUM_CLIENTS):
                client_primes[client_id][i][j] = tmp[i * NUM_CLIENTS + j]

    print_ln("Starting Evaluation Phase")

    # Computes evaluation, prioritization, mapping and selection phase
    matching_prime = compute_matching(ec_graphs, adjacency_matrix, client_primes, config_cp0, config_cp1)

    # Sends output prime to clients, who then run the reverse-mapping phase which has negligible runtime
    write_prime_to_clients(client_sockets, number_clients, matching_prime)

    print_ln("Finished Protocol Execution")


main()
