/*
 * CSIRO Open Source Software Licence Agreement (variation of the BSD / MIT License)
 * Copyright (c) 2021, Commonwealth Scientific and Industrial Research Organisation (CSIRO) ABN 41 687 119 230.
 * All rights reserved. CSIRO is willing to grant you a licence to this MP-SPDZ sofware on the following terms, except where otherwise indicated for third party material.
 * Redistribution and use of this software in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 * * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * * Neither the name of CSIRO nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission of CSIRO.
 * EXCEPT AS EXPRESSLY STATED IN THIS AGREEMENT AND TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, THE SOFTWARE IS PROVIDED "AS-IS". CSIRO MAKES NO REPRESENTATIONS, WARRANTIES OR CONDITIONS OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY REPRESENTATIONS, WARRANTIES OR CONDITIONS REGARDING THE CONTENTS OR ACCURACY OF THE SOFTWARE, OR OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, THE ABSENCE OF LATENT OR OTHER DEFECTS, OR THE PRESENCE OR ABSENCE OF ERRORS, WHETHER OR NOT DISCOVERABLE.
 * TO THE FULL EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL CSIRO BE LIABLE ON ANY LEGAL THEORY (INCLUDING, WITHOUT LIMITATION, IN AN ACTION FOR BREACH OF CONTRACT, NEGLIGENCE OR OTHERWISE) FOR ANY CLAIM, LOSS, DAMAGES OR OTHER LIABILITY HOWSOEVER INCURRED.  WITHOUT LIMITING THE SCOPE OF THE PREVIOUS SENTENCE THE EXCLUSION OF LIABILITY SHALL INCLUDE: LOSS OF PRODUCTION OR OPERATION TIME, LOSS, DAMAGE OR CORRUPTION OF DATA OR RECORDS; OR LOSS OF ANTICIPATED SAVINGS, OPPORTUNITY, REVENUE, PROFIT OR GOODWILL, OR OTHER ECONOMIC LOSS; OR ANY SPECIAL, INCIDENTAL, INDIRECT, CONSEQUENTIAL, PUNITIVE OR EXEMPLARY DAMAGES, ARISING OUT OF OR IN CONNECTION WITH THIS AGREEMENT, ACCESS OF THE SOFTWARE OR ANY OTHER DEALINGS WITH THE SOFTWARE, EVEN IF CSIRO HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH CLAIM, LOSS, DAMAGES OR OTHER LIABILITY.
 * APPLICABLE LEGISLATION SUCH AS THE AUSTRALIAN CONSUMER LAW MAY APPLY REPRESENTATIONS, WARRANTIES, OR CONDITIONS, OR IMPOSES OBLIGATIONS OR LIABILITY ON CSIRO THAT CANNOT BE EXCLUDED, RESTRICTED OR MODIFIED TO THE FULL EXTENT SET OUT IN THE EXPRESS TERMS OF THIS CLAUSE ABOVE "CONSUMER GUARANTEES".  TO THE EXTENT THAT SUCH CONSUMER GUARANTEES CONTINUE TO APPLY, THEN TO THE FULL EXTENT PERMITTED BY THE APPLICABLE LEGISLATION, THE LIABILITY OF CSIRO UNDER THE RELEVANT CONSUMER GUARANTEE IS LIMITED (WHERE PERMITTED AT CSIRO'S OPTION) TO ONE OF FOLLOWING REMEDIES OR SUBSTANTIALLY EQUIVALENT REMEDIES:
 * (a)               THE REPLACEMENT OF THE SOFTWARE, THE SUPPLY OF EQUIVALENT SOFTWARE, OR SUPPLYING RELEVANT SERVICES AGAIN;
 * (b)               THE REPAIR OF THE SOFTWARE;
 * (c)               THE PAYMENT OF THE COST OF REPLACING THE SOFTWARE, OF ACQUIRING EQUIVALENT SOFTWARE, HAVING THE RELEVANT SERVICES SUPPLIED AGAIN, OR HAVING THE SOFTWARE REPAIRED.
 * IN THIS CLAUSE, CSIRO INCLUDES ANY THIRD PARTY AUTHOR OR OWNER OF ANY PART OF THE SOFTWARE OR MATERIAL DISTRIBUTED WITH IT.  CSIRO MAY ENFORCE ANY RIGHTS ON BEHALF OF THE RELEVANT THIRD PARTY.
 */

/*
 * External client (input peer) sending input to and receiving output from the computing peers adapted to the use case
 * of kidney exchange.
 * The original source code can be found in:
 * https://github.com/data61/MP-SPDZ/blob/master/ExternalIO/bankers-bonus-client.cpp
 */

#include "Math/gfp.h"
#include "Math/gf2n.h"
#include "Networking/sockets.h"
#include "Networking/ssl_sockets.h"
#include "Tools/int.h"
#include "Math/Setup.h"
#include "Protocols/fake-stuff.h"

#include "Math/gfp.hpp"

#include <sodium.h>
#include <iostream>
#include <sstream>
#include <fstream>

#define NUM_CLIENTS 3
#define MAX_NUM_N_HOODS (NUM_CLIENTS * NUM_CLIENTS)
#define PROT_KEP_RND_SS "kep_rnd_ss"
#define PROT_CROSSOVER_KE "crossover_ke"

template<class T>
void send_private_input(const vector<T>& values, vector<ssl_socket*>& sockets, int nparties){
    int numInputs = values.size();
    octetStream os;
    vector<vector<T>> triples(numInputs, vector<T>(3));
    vector<T> triple_shares(3);

    // receive numInputs triples from SPDZ
    for(int j = 0; j < nparties; j++){
        os.reset_write_head();
        os.Receive(sockets[j]);

        for(int j = 0; j < numInputs; j++){
            for(int k = 0; k < 3; k++){
                triple_shares[k].unpack(os);
                triples[j][k] += triple_shares[k];
            }
        }
    }

    // check triple relations (is a party cheating?)
    for(int i = 0; i < numInputs; i++){
        if(T(triples[i][0] * triples[i][1]) != triples[i][2]){
            cerr << triples[i][2] << " != " << triples[i][0] << " * " << triples[i][1] << endl;
            cerr << "Incorrect triple at " << i << ", aborting\n";
            throw mac_fail();
        }
    }

    // send inputs + triple[0], so SPDZ can compute shares of each value
    os.reset_write_head();
    for(int i = 0; i < numInputs; i++){
        T y = values[i] + triples[i][0];
        y.pack(os);
    }

    for(int j = 0; j < nparties; j++){
        os.Send(sockets[j]);
    }
}

template<class T>
T receive_result(vector<ssl_socket*>& sockets, int nparties){
    vector<T> outputValues(3);
    octetStream os;
    for(int i = 0; i < nparties; i++){
        os.reset_write_head();
        os.Receive(sockets[i]);
        for(unsigned int j = 0; j < 3; j++){
            T value;
            value.unpack(os);
            outputValues[j] += value;
        }
    }

    if(T(outputValues[0] * outputValues[1]) != outputValues[2]){
        cerr << "Unable to authenticate output value as correct, aborting." << endl;
        throw mac_fail();
    }

    return outputValues[0];
}

template<class T>
void run(int num_bloodtypes, vector<int>& donor_blood_vec, vector<int>& patient_blood_vec, int num_antigens_a,
        vector<int>& donor_ant_a_vec, vector<int>& patient_ant_a_vec, int num_antigens_b, vector<int>& donor_ant_b_vec,
        vector<int>& patient_ant_b_vec, int num_antigens_c, vector<int>& donor_ant_c_vec, vector<int>& patient_ant_c_vec,
        int num_antigens_dq, vector<int>& donor_ant_dq_vec, vector<int>& patient_ant_dq_vec, int num_antigens_dr,
        vector<int>& donor_ant_dr_vec, vector<int>& patient_ant_dr_vec, vector<int>& n_hoods_vec,
        vector<ssl_socket*>& sockets, int nparties, int myClientID, string protocol){

    vector<T> conv_donor_blood;
    for(int i = 0; i < num_bloodtypes; i++){
        conv_donor_blood.push_back(donor_blood_vec[i]);
    }

    // send donor bloodtypes
    send_private_input<T>(conv_donor_blood, sockets, nparties);

    vector<T> conv_patient_blood;
    for(int i = 0; i < num_bloodtypes; i++){
        conv_patient_blood.push_back(patient_blood_vec[i]);
    }

    // send patient bloodtypes
    send_private_input<T>(conv_patient_blood, sockets, nparties);


    vector<T> conv_donor_ant_a;
    for(int i = 0; i < num_antigens_a; i++){
        conv_donor_ant_a.push_back(donor_ant_a_vec[i]);
    }

    // send donor A antigens
    send_private_input<T>(conv_donor_ant_a, sockets, nparties);

    vector<T> conv_patient_ant_a;
    for(int i = 0; i < num_antigens_a; i++){
        conv_patient_ant_a.push_back(patient_ant_a_vec[i]);
    }

    // send patient A antibodies
    send_private_input<T>(conv_patient_ant_a, sockets, nparties);


    vector<T> conv_donor_ant_b;
    for(int i = 0; i < num_antigens_b; i++){
        conv_donor_ant_b.push_back(donor_ant_b_vec[i]);
    }

    // send donor B antigens
    send_private_input<T>(conv_donor_ant_b, sockets, nparties);

    vector<T> conv_patient_ant_b;
    for(int i = 0; i < num_antigens_b; i++){
        conv_patient_ant_b.push_back(patient_ant_b_vec[i]);
    }

    // send patient B antibodies
    send_private_input<T>(conv_patient_ant_b, sockets, nparties);


    vector<T> conv_donor_ant_c;
    for(int i = 0; i < num_antigens_c; i++){
        conv_donor_ant_c.push_back(donor_ant_c_vec[i]);
    }

    // send donor C antigens
    send_private_input<T>(conv_donor_ant_c, sockets, nparties);

    vector<T> conv_patient_ant_c;
    for(int i = 0; i < num_antigens_c; i++){
        conv_patient_ant_c.push_back(patient_ant_c_vec[i]);
    }

    // send patient C antibodies
    send_private_input<T>(conv_patient_ant_c, sockets, nparties);


    vector<T> conv_donor_ant_dq;
    for(int i = 0; i < num_antigens_dq; i++){
        conv_donor_ant_dq.push_back(donor_ant_dq_vec[i]);
    }

    // send donor DQ antigens
    send_private_input<T>(conv_donor_ant_dq, sockets, nparties);

    vector<T> conv_patient_ant_dq;
    for(int i = 0; i < num_antigens_dq; i++){
        conv_patient_ant_dq.push_back(patient_ant_dq_vec[i]);
    }

    // send patient DQ antibodies
    send_private_input<T>(conv_patient_ant_dq, sockets, nparties);


    vector<T> conv_donor_ant_dr;
    for(int i = 0; i < num_antigens_dr; i++){
        conv_donor_ant_dr.push_back(donor_ant_dr_vec[i]);
    }

    // send donor DR antigens
    send_private_input<T>(conv_donor_ant_dr, sockets, nparties);

    vector<T> conv_patient_ant_dr;
    for(int i = 0; i < num_antigens_dr; i++){
        conv_patient_ant_dr.push_back(patient_ant_dr_vec[i]);
    }

    // send patient DR antibodies
    send_private_input<T>(conv_patient_ant_dr, sockets, nparties);

    if(protocol == PROT_KEP_RND_SS){
        vector<T> conv_neighbourhoods;
        for(int i = 0; i < MAX_NUM_N_HOODS; i++){
            conv_neighbourhoods.push_back(n_hoods_vec[i]);
        }

        // send primes for neighbourhoods
        send_private_input<T>(conv_neighbourhoods, sockets, nparties);
    }


    cout << "Sent private inputs to each SPDZ engine, waiting for result..." << endl;

    if(protocol == PROT_KEP_RND_SS){
        // get the result back
        bigint prime_product_bi = 0;
        const T prime_product = receive_result<T>(sockets, nparties);
        to_signed_bigint(prime_product_bi, prime_product);

        // Search for own prime divisor of returned prime_product
        if(prime_product_bi == 0){
            cout << "No suitable exchange constellation graph could be found" << endl;
        }else{
            bool divisor_found = false; // for breaking out of the 2 loops
            cout << "Calculating Exchange Partner from Prime..." << endl;
            for(int from = 0; from < NUM_CLIENTS && !divisor_found; from++){
                for(int to = 0; to < NUM_CLIENTS && !divisor_found; to++){
                    const bigint current_n_hood = n_hoods_vec[from * NUM_CLIENTS + to];
                    if(gcd(prime_product_bi, current_n_hood) == current_n_hood){
                        if(from == myClientID && to == myClientID){
                            cout << "There was no match for your patient-donor pair" << endl;
                        }else{
                            cout << "The donor for your patient is: " << (from + 1) << endl;
                            cout << "The recipient for your donor is: " << (to + 1) << endl;
                        }

                        divisor_found = true;
                    }
                }
            }
        }
    }else{
        // get the donor
        T donor = receive_result<T>(sockets, nparties);
        // get the recipient
        T recipient = receive_result<T>(sockets, nparties);

        if(donor == 0){
            cout << "There was no match for your patient-donor pair" << endl;
        }else{
            cout << "The donor for your patient is: " << donor << endl;
            cout << "The recipient for your donor is: " << recipient << endl;
        }
    }
}

int main(int argc, char** argv){
    int myClientID;
    int nparties;
    int inputSize  = 172;
    int num_bloodtypes = 4;
    int num_antigens_a = 21;
    int num_antigens_b = 35;
    int num_antigens_c = 8;
    int num_antigens_dq = 5;
    int num_antigens_dr = 13;
    int finish;
    int portBase = 14000;
    string hostName = "localhost";
    string protocol;

    if(argc < 4){
        cout << "Usage is external_client <client-identifier> <number of spdz parties> <finish (0: false, 1: true)> <protocol name>"
            << "<optional: host name, default: localhost> <optional: spdz party port base number, default: 14000>" << endl;
        exit(0);
    }

    myClientID = atoi(argv[1]);
    nparties = atoi(argv[2]);
    finish = atoi(argv[3]);
    protocol = argv[4];
    if(argc > 5){
        hostName = argv[5];
    }
    if(argc > 6){
        portBase = atoi(argv[6]);
    }

    if(protocol == PROT_KEP_RND_SS){
        inputSize = inputSize + MAX_NUM_N_HOODS + 1;
    }

    string inputArray[inputSize];

    string filename = "ExternalIO/Inputs/input_"+protocol+"_"+to_string(myClientID)+".txt";

    ifstream file(filename);
    if(file.is_open()){
        for(int i = 0; i < inputSize; i++){
            file >> inputArray[i];
        }
    }

    vector<int> inputVector (inputSize, 0);
    for(int i = 0; i < inputSize; i++){
        inputVector[i] = stoi(inputArray[i]);
    }

    vector<int> donor_blood_vec (num_bloodtypes, 0);
    for(int i = 0; i < num_bloodtypes; i++){
        donor_blood_vec[i] = inputVector[i];
    }

    vector<int> donor_ant_a_vec (num_antigens_a, 0);
    for(int i = 0; i < num_antigens_a; i++){
        donor_ant_a_vec[i] = inputVector[num_bloodtypes + i];
    }

    vector<int> donor_ant_b_vec (num_antigens_b, 0);
    for(int i = 0; i < num_antigens_b; i++){
        donor_ant_b_vec[i] = inputVector[num_bloodtypes + num_antigens_a + i];
    }

    vector<int> donor_ant_c_vec (num_antigens_c, 0);
    for(int i = 0; i < num_antigens_c; i++){
        donor_ant_c_vec[i] = inputVector[num_bloodtypes + num_antigens_a + num_antigens_b + i];
    }

    vector<int> donor_ant_dq_vec (num_antigens_dq, 0);
    for(int i = 0; i < num_antigens_dq; i++){
        donor_ant_dq_vec[i] = inputVector[num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + i];
    }

    vector<int> donor_ant_dr_vec (num_antigens_dr, 0);
    for(int i = 0; i < num_antigens_dr; i++){
        donor_ant_dr_vec[i] = inputVector[num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + num_antigens_dq + i];
    }

    vector<int> patient_blood_vec (num_bloodtypes, 0);
    for(int i = 0; i < num_bloodtypes; i++){
        patient_blood_vec[i] = inputVector[num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + num_antigens_dq + num_antigens_dr + i];
    }

    vector<int> patient_ant_a_vec (num_antigens_a, 0);
    for(int i = 0; i < num_antigens_a; i++){
        patient_ant_a_vec[i] = inputVector[num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + num_antigens_dq + num_antigens_dr + num_bloodtypes + i];
    }

    vector<int> patient_ant_b_vec (num_antigens_b, 0);
    for(int i = 0; i < num_antigens_b; i++){
        patient_ant_b_vec[i] = inputVector[num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + num_antigens_dq + num_antigens_dr + num_bloodtypes + num_antigens_a + i];
    }

    vector<int> patient_ant_c_vec (num_antigens_c, 0);
    for(int i = 0; i < num_antigens_c; i++){
        patient_ant_c_vec[i] = inputVector[num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + num_antigens_dq + num_antigens_dr + num_bloodtypes + num_antigens_a + num_antigens_b + i];
    }

    vector<int> patient_ant_dq_vec (num_antigens_dq, 0);
    for(int i = 0; i < num_antigens_dq; i++){
        patient_ant_dq_vec[i] = inputVector[num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + num_antigens_dq + num_antigens_dr + num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + i];
    }

    vector<int> patient_ant_dr_vec (num_antigens_dr, 0);
    for(int i = 0; i < num_antigens_dr; i++){
        patient_ant_dr_vec[i] = inputVector[num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + num_antigens_dq + num_antigens_dr + num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + + num_antigens_dq + i];
    }

    vector<int> n_hoods_vec (MAX_NUM_N_HOODS, 0);
    if(protocol == PROT_KEP_RND_SS){
        for(int i = 0; i < MAX_NUM_N_HOODS; i++){
            n_hoods_vec[i] = inputVector[num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + num_antigens_dq + num_antigens_dr + num_bloodtypes + num_antigens_a + num_antigens_b + num_antigens_c + + num_antigens_dq + num_antigens_dr + i];
        }
    }

    bigint::init_thread();

    // setup connections from this client to each party socket
    vector<int> plainSockets(nparties);
    vector<ssl_socket*> sockets(nparties);
    ssl_ctx ctx("C" + to_string(myClientID));
    ssl_service ioService;
    octetStream specification;
    for(int i = 0; i < nparties; i++){
        set_up_client_socket(plainSockets[i], hostName.c_str(), portBase + i);

        send(plainSockets[i], (octet*) &myClientID, sizeof(int));
        sockets[i] = new ssl_socket(ioService, ctx, plainSockets[i], "P" + to_string(i), "C" + to_string(myClientID), true);
        if(i == 0){
            specification.Receive(sockets[0]);
        }
        octetStream os;
        os.store(finish);
        os.Send(sockets[i]);
    }
    cout << "Finish setup socket connections to SPDZ engines." << endl;

    int type = specification.get<int>();
    switch (type) {
        case 'p':
            gfp::init_field(specification.get<bigint>());
            cout << "using prime " << gfp::pr() << endl;


            run<gfp>(num_bloodtypes, donor_blood_vec, patient_blood_vec, num_antigens_a, donor_ant_a_vec,
                    patient_ant_a_vec, num_antigens_b, donor_ant_b_vec, patient_ant_b_vec, num_antigens_c,
                    donor_ant_c_vec, patient_ant_c_vec, num_antigens_dq, donor_ant_dq_vec, patient_ant_dq_vec,
                    num_antigens_dr, donor_ant_dr_vec, patient_ant_dr_vec, n_hoods_vec, sockets, nparties, myClientID,
                    protocol);
            break;
        default:
            cerr << "Type " << type << " not implemented.";
            exit(1);
    }

    for(int i = 0; i < nparties; i++){
        delete sockets[i];
    }

    return 0;
}
