import glob
import os
import shutil
import tarfile
import sys


def download_mpspdz(url):
    """
    Write the file contained in the downloadURL url into the current directory. Return the filename of the written file.
    """
    local_filename = url.split('/')[-1]
    os.system("wget -O " + local_filename + " " + url)
    return local_filename


def extract_tar(name):
    """
    Extract the MP-SPDZ code, delete the archive and rename the extracted folder to `MPSPDZ`.
    """

    with tarfile.open(name) as tar:
        tar.extractall("./")
    os.remove(name)
    folder = glob.glob("mp-spdz-*")[0]
    os.rename(folder, "MPSPDZ")


def copy_deltas():
    """
    Move files specified in deltas.txt into MPSPDZ.
    This allows for custom files to be moved into the updated and reset repository automatically.
    """
    with open("protocols/deltas.txt", "r") as deltas:
        for line in deltas:

            target = line.split(">")
            target = [elem.strip() for elem in target]

            try:
                shutil.copytree(target[0], target[1])
            except NotADirectoryError:
                shutil.copy(target[0], target[1])


def main():
    """
    Download version 0.2.5 of the MP-SPDZ repo and extract it.
    This is the version the protocol was developed for.
    Later versions could lead to the protocol failing to compile/execute.
    """
    print("Setting up environment...")
    latest = "https://github.com/data61/MP-SPDZ/releases/download/v0.2.5/mp-spdz-0.2.5.tar.xz"

    print("Downloading release...")
    print("This can take a minute...")
    filename = download_mpspdz(latest)

    print("Extracting files...")
    extract_tar(filename)

    print("Moving files to appropriate folders...")
    copy_deltas()


if __name__ == "__main__":
    os.chdir(sys.path[0])
    main()
