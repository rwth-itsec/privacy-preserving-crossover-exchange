import os
import random
import sys
from utils import setup_scripts, execute, copy_inputs

COMPUTING_PEERS = 3


def compile_mpc_file(input_peers):
    """
    Conducts compilation of just the *.mpc file. Replace the number of nodes inside the file.
    """

    print("Compiling `crossover_ke`")
    with open("protocols/Programs/Source/crossover_ke.mpc", "r") as file:
        lines = file.readlines()
    lines[16] = f"NUM_NODES = {input_peers}\n"
    with open("protocols/Programs/Source/crossover_ke.mpc", "w") as file:
        file.writelines(lines)
    with open("protocols/Compiler/comp_gate.py", "r") as file:
        lines = file.readlines()
    lines[12] = f"NUM_NODES = {input_peers}\n"
    with open("protocols/Compiler/comp_gate.py", "w") as file:
        file.writelines(lines)

    execute(["cp", "protocols/Compiler/comp_gate.py", "MPSPDZ/Compiler/"], "./", "\n\nCopying Dependencies")

    execute(["../MPSPDZ/compile.py", "crossover_ke"], "./protocols",
            "\n\nExecuting /MPSPDZ/compile_crossover_ke.py crossover_ke")


def generate_random_input(input_peers):
    """
    Create random example inputs for those input peers for which no input data is specified under 'protocols/Inputs'.
    """

    for i in range(input_peers):
        f_name = f"./MPSPDZ/ExternalIO/Inputs/input_crossover_ke_{i}.txt"
        if os.path.isfile(f_name):
            continue
        with open(f_name, "w") as file:
            # meaning: Bloodtype indicator, HLA-A, -B, -C, -DQ, -DR
            donor_lengths = [4, 21, 35, 8, 5, 13]

            for k in donor_lengths:
                if k == 4:
                    rnd = random.randint(0, 3)
                    if rnd == 0:
                        donor_bloodtype_indicator = "1 1 1 1\n"
                    elif rnd == 1:
                        donor_bloodtype_indicator = "0 1 0 1\n"
                    elif rnd == 2:
                        donor_bloodtype_indicator = "0 0 1 1\n"
                    else:
                        donor_bloodtype_indicator = "0 0 0 1\n"
                    file.write(donor_bloodtype_indicator)
                else:
                    for j in range(k):
                        file.write("0 ")
                    file.write("\n")

            # meaning: Bloodtype indicator, HLA-A, -B, -C, -DQ, -DR
            patient_lengths = [4, 21, 35, 8, 5, 13]

            for k in patient_lengths:
                if k == 4:
                    rnd = random.randint(0, 3)
                    if rnd == 0:
                        patient_bloodtype_indicator = "1 0 0 0\n"
                    elif rnd == 1:
                        patient_bloodtype_indicator = "0 1 0 1\n"
                    elif rnd == 2:
                        patient_bloodtype_indicator = "0 0 1 1\n"
                    else:
                        patient_bloodtype_indicator = "1 1 1 1\n"
                    file.write(patient_bloodtype_indicator)
                else:
                    for j in range(k):
                        file.write("0 ")
                    file.write("\n")


def make_external_client():
    """
    Compile the external client file which manages the input peers.
    """

    print("Making external clients to provide input")

    execute(["make", 'external-client.x'], "./MPSPDZ",
            "\n\nExecuting 'make external-client.x'")


def main(input_peers=3):
    """
    Compile all necessary files to run the protocol Crossover KE.
    """

    copy_inputs()

    generate_random_input(input_peers)

    setup_scripts(int(input_peers), COMPUTING_PEERS)

    make_external_client()

    compile_mpc_file(input_peers)


if __name__ == "__main__":
    os.chdir(sys.path[0])
    if len(sys.argv) > 1:
        try:
            main(int(sys.argv[1]))
        except ValueError:
            print("Please enter a valid number of input peers as parameter (or none).")
    else:
        main()
