import subprocess


def copy_inputs():
    """
    Copy the inputs of all input peers to the MP-SPDZ folder.
    """
    try:
        execute(["rm", "-r", "./ExternalIO/Inputs/"], "./MPSPDZ/", "\n\nRemoving old Input Data")
    except subprocess.CalledProcessError:
        print("ERROR: No old Input Data available.\n\n")

    execute(["cp", "-r", "../protocols/Inputs/", "./ExternalIO/"], "./MPSPDZ/", "\n\nCopying Input Data")


def execute(cmd, currwd, prnt):
    """
    Helper method. Wraps around subprocess Popen. Executes one command after printing a descriptor.
    """

    print(prnt)

    popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, cwd=currwd, universal_newlines=True)

    for line in popen.stdout:
        print(line, end='')

    popen.stdout.close()
    return_code = popen.wait()

    if return_code:
        raise subprocess.CalledProcessError(return_code, cmd)


def setup_scripts(input_peers, computing_peers):
    """
    Run the general setup scripts of MP-SPDZ.
    """
    try:
        execute(["rm", "-r", "./Player-Data/", "../protocols/Player-Data/"], "./MPSPDZ/",
                "\n\nRemoving old Player Data")

    except subprocess.CalledProcessError:
        print("ERROR: No old Player Data available.\n\n")

    try:
        execute(["./Scripts/tldr.sh"], "./MPSPDZ/", "\n\nExecuting 'tldr.sh'")

        execute(["./Scripts/setup-ssl.sh", str(computing_peers)], "./MPSPDZ/", "\n\nExecuting 'setup-ssl.sh'")

        execute(["./Scripts/setup-clients.sh", str(input_peers)], "./MPSPDZ/", "\n\nExecuting 'setup-clients.sh'")

    except subprocess.CalledProcessError:
        print(
            "ERROR: MPSPDZ setup scripts returned exit status 1. If this is your first compilation run please abort "
            "and fix here.\n\n")

    execute(["cp", "-r", "../MPSPDZ/Player-Data/", "."], "./protocols", "\n\nCopying Player Data to protocols")
