import os
import random
import sys
import ecgs_computation.Scripts.primes as primes
import ecgs_computation.ECGS.gen_ecgs_2_cycles as gen_2_cycles
import ecgs_computation.ECGS.gen_ecgs_3_cycles as gen_3_cycles
from utils import copy_inputs, execute, setup_scripts

MAX_NUM_INPUT_PEERS = 15
COMPUTING_PEERS = 3


def compile_mpc_file(input_peers, max_cycle_length):
    """
    Conducts compilation of just the *.mpc file. Replace the number of nodes inside the file.
    """

    print("Compiling `kep_rnd_ss`")
    with open("protocols/Programs/Source/kep_rnd_ss.mpc", "r") as file:
        lines = file.readlines()
    lines[23] = f"NUM_CLIENTS = {input_peers}\n"
    lines[26] = f"MAX_CYCLE_SIZE = {max_cycle_length}\n"
    with open("protocols/Programs/Source/kep_rnd_ss.mpc", "w") as file:
        file.writelines(lines)
    with open("protocols/Compiler/comp_gate.py", "r") as file:
        lines = file.readlines()
    lines[12] = f"NUM_NODES = {input_peers}\n"
    with open("protocols/Compiler/comp_gate.py", "w") as file:
        file.writelines(lines)

    execute(["cp", "protocols/Compiler/comp_gate.py", "MPSPDZ/Compiler/"], "./", "\n\nCopying Dependencies")

    execute(["../MPSPDZ/compile.py", "kep_rnd_ss"], "./protocols",
            "\n\nExecuting /MPSPDZ/compile_crossover_ke.py kep_rnd_ss")

    execute(["cp", "-r", f"ecgs_computation/ECGS/Graphs/{get_public_input_file(input_peers, max_cycle_length)}",
             "protocols/Programs/Public-Input/kep_rnd_ss"],
            ".", "\n\nCopying public Computing Peer Input.")


def generate_random_input(input_peers):
    """
    Create example client data, for non existing, but required input nodes.
    """
    n_hood_primes = primes.generate_n_hoods(MAX_NUM_INPUT_PEERS)

    for i in range(input_peers):
        f_name = f"./MPSPDZ/ExternalIO/Inputs/input_kep_rnd_ss_{i}.txt"
        if os.path.isfile(f_name):
            # Copies file content, but changes primes
            with open(f_name, "r") as file:
                lines = file.readlines()
            lines[12] = ""
            for prime in n_hood_primes[i]:
                lines[12] += f"{prime} "
            lines[12] += "\n"
            with open(f_name, "w") as file:
                file.writelines(lines)
        else:
            with open(f_name, "w") as file:
                # meaning: Bloodtype indicator, HLA-A, -B, -C, -DQ, -DR
                donor_lengths = [4, 21, 35, 8, 5, 13]

                for k in donor_lengths:
                    if k == 4:
                        rnd = random.randint(0, 3)
                        if rnd == 0:
                            donor_bloodtype_indicator = "1 1 1 1\n"
                        elif rnd == 1:
                            donor_bloodtype_indicator = "0 1 0 1\n"
                        elif rnd == 2:
                            donor_bloodtype_indicator = "0 0 1 1\n"
                        else:
                            donor_bloodtype_indicator = "0 0 0 1\n"
                        file.write(donor_bloodtype_indicator)
                    else:
                        for j in range(k):
                            file.write("0 ")
                        file.write("\n")

                # meaning: Bloodtype indicator, HLA-A, -B, -C, -DQ, -DR
                patient_lengths = [4, 21, 35, 8, 5, 13]

                for k in patient_lengths:
                    if k == 4:
                        rnd = random.randint(0, 3)
                        if rnd == 0:
                            patient_bloodtype_indicator = "1 0 0 0\n"
                        elif rnd == 1:
                            patient_bloodtype_indicator = "0 1 0 1\n"
                        elif rnd == 2:
                            patient_bloodtype_indicator = "0 0 1 1\n"
                        else:
                            patient_bloodtype_indicator = "1 1 1 1\n"
                        file.write(patient_bloodtype_indicator)
                    else:
                        for j in range(k):
                            file.write("0 ")
                        file.write("\n")

                print(f"Generating Primes for Client {i}")
                for prime in n_hood_primes[i]:
                    file.write(str(prime) + " ")


def get_public_input_file(input_peers, max_cycle_length):
    if max_cycle_length == 2:
        return "ecgs_c2_" + str(input_peers)
    elif max_cycle_length == 3:
        return "ecgs_c3_" + str(input_peers)
    else:
        print("The maximum cycle length must be 2 or 3. Aborting.")
        exit(1)


def generate_ecgs(input_peers, max_cycle_length):
    f_name = f"./ecgs_computation/ECGS/Graphs/ecgs_c{max_cycle_length}_{input_peers}"
    if not os.path.isfile(f_name):
        print("Generating ECGS for "+str(input_peers)+" input peers and max. cycle length "+str(max_cycle_length))
        graphs_dir = "ecgs_computation/ECGS/Graphs/"
        if max_cycle_length == 2:
            gen_2_cycles.generate_cycles_two(input_peers, graphs_dir)
        elif max_cycle_length == 3:
            gen_3_cycles.generate_cycles_three(input_peers, graphs_dir)
        else:
            print("The maximum cycle length must be 2 or 3. Aborting.")
            exit(1)


def make_external_client(input_peers):
    """
    Conducts compilation of just the *.x file.
    """

    print("Making external input_peers to provide input")

    with open("protocols/external-client.cpp", "r") as file:
        lines = file.readlines()
    lines[39] = f"#define NUM_CLIENTS {input_peers}\n"
    with open("protocols/external-client.cpp", "w") as file:
        file.writelines(lines)

    execute(
        ["cp", "-r", "protocols/external-client.cpp", "MPSPDZ/ExternalIO/external-client.cpp"],
        ".", "\nCopying external-client to MPSPDZ/ExternalIO")

    execute(["make", 'external-client.x'], "./MPSPDZ", "\n\nExecuting 'make external-client.x'")


def main(input_peers=3, max_cycle_length=2):
    """
    Main method. Runs through scripts and make commands in order to compile the different MPSPDZ files needed.
    """

    if max_cycle_length not in [2, 3]:
        print("Maximum cycle length must be either 2 or 3. Aborting")
        exit(1)

    copy_inputs()

    generate_random_input(input_peers)

    setup_scripts(input_peers, COMPUTING_PEERS)

    generate_ecgs(input_peers, max_cycle_length)

    make_external_client(input_peers)

    compile_mpc_file(input_peers, max_cycle_length)


if __name__ == "__main__":
    os.chdir(sys.path[0])
    if len(sys.argv) > 1:
        try:
            main(int(sys.argv[1]), int(sys.argv[2]))
        except IndexError:
            print("Please enter 2 arguments: Number of input peers, Maximum cycle length (either 2 or 3)")
    else:
        main()
