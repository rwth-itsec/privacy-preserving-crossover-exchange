# Privacy-Preserving Maximum Matching on General Graphs and its Application to Enable Privacy-Preserving Kidney Exchange

This repository contains the code for the privacy-preserving protocols for kidney exchange presented in the paper *"Privacy-Preserving Maximum Matching on General Graphs and its Application to Enable Privacy-Preserving Kidney Exchange"* [[1]](#1).


## Setup

### Python Requirements
 - Python 3.8 or higher
 - Packages listed in requirements.txt: `pip install -r requirements.txt`
 
 The assumption in this file is that python is installed as `python` and pip as `pip`.

### Setup of MP-SPDZ
Our protocols are implemented using the secure multi-party computation benchmarking framework [MP-SPDZ](https://github.com/data61/MP-SPDZ). The following requirements have to be installed to be able to use MP-SPDZ:

 - GCC 5 or later (tested with up to 10) or LLVM/clang 5 or later (tested with up to 11). 
 - MPIR library, compiled with C++ support (use flag `--enable-cxx` when running configure). You can use `make -j8 tldr` to install it locally.
 - libsodium library, tested against 1.0.16
 - OpenSSL, tested against 1.1.1
 - Boost.Asio with SSL support (`libboost-dev` on Ubuntu), tested against 1.65
 - Boost.Thread for BMR (`libboost-thread-dev` on Ubuntu), tested against 1.65
 - 64-bit CPU
 - Python 3.5 or later
 - NTL library for homomorphic encryption (optional; tested with NTL 10.5)
 - The wget command
 - If using macOS, Sierra or later

After installing the above requirements, execute: `python setup.py`

This downloads the correct version of MP-SPDZ and sets up all remaining files for running the protocols.


## Running the protocols

#### Input encoding
For each protocol there are three example input files in the directory `protocols/Inputs/`. Each input file contains bloodtype and antigen indicator vectors for the donor in the first 6 rows and for the patient in rows 7 to 12. The first row always indicates the bloodtype whereas the following 5 rows indicate the donor's (patient's) antigens (antibodies) for the HLA loci A, B, C, DQ, and DR. The input files for the protocol KEP-Rnd-SS additionally contain a set of prime numbers in the last row. These are used to recover the exchange partners from the output provided by protocol KEP-Rnd-SS. For details, we refer to the source code or to Breuer et al. [[2]](#2) for the original specififcation of protocol KEP-Rnd based on homomorphic encryption.  If the protocols are run for more than three input peers, additional files containing random bloodtypes and only zeros in the antigen indicator vectors are generated. Alternatively, one can use the template files provided in `protocols/Inputs/` to generate own input files. 

#### Run protocol Crossover-KE 
Execute the following steps to run the protocol Crossover-KE which computes the optimal crossover exchanges for a set of input peers using our privacy-preserving implementation of the matching algorithm by Pape and Conradt [[3]](#3). For further details on the protocol specification we refer to the source code itself or to our paper [[1]](#1).

1. Compile the protocol: `python compile_crossover_ke.py <number of input peers>`

2. Run the protocol: `python run_crossover_ke.py <number of input peers>`

If you do not explicitly specify a number of input peers, the protocol is run in its default configuration with three input peers. Note that you always have to execute `compile_crossover_ke.py` before running the protocol for a different number of input peers or for different input data. 

The protocol output is printed to the command line and it indicates the exchange partner for patient and donor of each patient-donor pair. If the pair is not part of an exchange, this is also stated in the output. Note that the output can differ for the same inputs due to the random shuffling of the adjacency matrix at the beginning of the protocol execution. 

#### Run protocol KEP-Rnd-SS 
Execute the following steps to run the protocol KEP-Rnd-SS which is the implementation of our privacy-preserving kidney exchange protocol from [[2]](#2) based on Secret Sharing instead of Homomorphic Encryption. 

1. Compile the protocol: `python compile_kep_rnd_ss.py <number of input peers> <maximum cycle size>`

2. Run the protocol: `python run_kep_rnd_ss.py <number of input peers> <maximum cycle size>`

As for protocol Crossover KE, the default configuration is to run the protocol with three input peers. Note that this protocol takes the maximum cycle size as additional input. Currently, the implementation supports cycle sizes of 2 and 3. Also note that the upper number of input peers for protocol KEP-Rnd-SS is currently 15 due to the large set of exchange constellation graphs that has to be generated. Finally, you always have to execute `compile_kep_rnd_ss.py` if you want to run the protocol for a different number of input peers, a different maximum cycle size, or with different input files.

## References
<a id="1">[1]</a> Malte Breuer, Ulrike Meyer, and Susanne Wetzel. 2022. Privacy-Preserving Maximum Matching on General Graphs and its Application to Enable Privacy-Preserving Kidney Exchange. In Proceedings of the Twelveth ACM Conference on Data and Application Security and Privacy (CODASPY 2022), April 24-27, Baltimore, MD, USA (to be published, a preprint is available at https://arxiv.org/abs/2201.06446).

<a id="2">[2]</a> Malte Breuer, Ulrike Meyer, Susanne Wetzel, and Anja Mühlfeld. 2020. A Privacy-Preserving Protocol for the Kidney Exchange Problem. In Workshop on Privacy in the Electronic Society. ACM.

<a id="3">[3]</a> U. Pape and D. Conradt. 1980. Maximales Matching in Graphen. Ausgewählte Operations Research Software in FORTRAN (1980).


## Acknowledgements

This work was funded by the Deutsche Forschungsgemeinschaft (DFG, German Resarch Foundation) - project number (419340256) and NSF grant CCF-1646999. Any opinion, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation.


## Licenses
This repository contains licensed code, here is a comprehensive list of all third party code used:

#### MP-SPDZ
Our protocols are implemented using the secure multi-party computation benchmarking framework MP-SPDZ :

- MP-SPDZ: Copyright (c) 2021, Commonwealth Scientific and Industrial Research Organisation (CSIRO) ABN 41 687 119 230. Licensed under CSIRO Open Source Software Licence Agreement (variation of the BSD / MIT License), see https://github.com/data61/MP-SPDZ/blob/v0.2.5/License.txt for details.

Besides using MP-SPDZ as the underlying framework for our protocols, we adapted the client-server infrastructure that MP-SPDZ (https://github.com/data61/MP-SPDZ) uses for External IO to our use case. In particular, our files `protocols/external-client.cpp` and `protocols/Programs/Compiler/networking.py` for client communication are adapted from the *bankers bonus* program provided by MP-SPDZ (https://github.com/data61/MP-SPDZ/blob/v0.2.5/ExternalIO/bankers-bonus-client.cpp and https://github.com/data61/MP-SPDZ/blob/v0.2.5/Programs/Source/bankers_bonus.mpc). The code was modified in some places to adapt it to the specific inputs and outputs of our kidney exchange protocols. Furthermore, we extended the Makefile of MP-SPDZ (https://github.com/data61/MP-SPDZ/blob/v0.2.5/Makefile) such that it also includes the instructions to compile our client code (c.f. file `protocols/Makefile`). The license for MP-SPDZ can be found in the files themselves as well as in the linked repository (https://github.com/data61/MP-SPDZ/blob/v0.2.5/License.txt).