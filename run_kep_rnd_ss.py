import subprocess
import ecgs_computation.Scripts.setup_players as setup_players
import sys
import os


NUM_COMPUTING_PEERS = 3
WARNING_COLOR = "\033[92m"
END_COLOR = "\033[0m"
OUTPUT_COLORS = ["\033[92m", "\033[94m", "\033[95m", "\033[96m", "\033[91m"]


def main():
    """
    Run the MP-SPDZ code for the protocol KEP-Rnd-SS based on the protocol KEP-Rnd presented in "Malte Breuer,
    Ulrike Meyer, Anja Mühlfeld and Susanne Wetzel. 2020. A Privacy-Preserving Protocol for the Kidney Exchange
    Problem. In Workshop on Privacy in the Electronic Society (WPES 2020)".
    This includes starting the protocol execution on all three computing peers as well as sending input and
    receiving output for all input peers. The output is printed to the terminal.
    """
    os.chdir(sys.path[0])
        
    if len(sys.argv) > 1:
        clients = int(sys.argv[1])
    else:
        clients = 3

    if len(sys.argv) > 2:
        max_cycle_length = int(sys.argv[2])
    else:
        max_cycle_length = 2

    program = "kep_rnd_ss"

    # Run the protocol KEP-Rnd-SS on the computing peers
    if max_cycle_length in [0, 2, 3]:
        setup_players.generate_waksman_config(clients, 0, max_cycle_length)
        setup_players.generate_waksman_config(clients, 1, max_cycle_length)
    else:
        print("The maximum cycle length must be 0, 2 or 3. Aborting")
        exit(1)

    popen_first = subprocess.Popen(
        ["../MPSPDZ/shamir-party.x", "-N", str(NUM_COMPUTING_PEERS), "-h", "localhost", "0", program],
        stdout=subprocess.PIPE, cwd="./protocols", universal_newlines=True)

    for i in range(1, NUM_COMPUTING_PEERS):
        subprocess.Popen(
            ["../MPSPDZ/shamir-party.x", "-N", str(NUM_COMPUTING_PEERS), "-h", "localhost", str(i), program],
            stdout=subprocess.DEVNULL, cwd="./protocols", universal_newlines=True)

    # Send the input of the input peers.
    popen_clients = []

    for i in range(int(clients)):
        if i == (int(clients) - 1):
            popen_clients.append(
                subprocess.Popen(["./external-client.x", str(i), str(NUM_COMPUTING_PEERS), "1", str(program)], stdout=subprocess.PIPE,
                                 cwd="./MPSPDZ", universal_newlines=True))
        else:
            popen_clients.append(
                subprocess.Popen(["./external-client.x", str(i), str(NUM_COMPUTING_PEERS), "0", str(program)], stdout=subprocess.PIPE,
                                 cwd="./MPSPDZ", universal_newlines=True))

    for line in popen_first.stdout:
        print(f"{WARNING_COLOR}{line}{END_COLOR}", end='')

    # Print the output of the input peers.
    for i in range(len(popen_clients)):
        for line in popen_clients[i].stdout:
            out = "Client " + str(i + 1) + ": " + line
            print(f"{OUTPUT_COLORS[i % len(OUTPUT_COLORS)]}{out}{END_COLOR}", end='')


if __name__ == "__main__":
    main()
