import subprocess
import sys
import os
import numpy as np

NUM_COMPUTING_PEERS = 3
WARNING_COLOR = "\033[92m"
END_COLOR = "\033[0m"
OUTPUT_COLORS = ["\033[92m", "\033[94m", "\033[95m", "\033[96m", "\033[91m"]


def generate_shuffle_input(input_peers):
    """
    Generate the random permutation for computing peers 0 and 1. These are read in during the protocol and used to
    shuffle and unshuffle the adjacency matrix / the solution matrix.
    """
    for peer in [0, 1]:
        perm = list(np.random.permutation(input_peers))
        dst = f"protocols/Player-Data/Input-P{peer}-0"
        perm_matrix = np.zeros((input_peers, input_peers))

        for i in range(input_peers):
            for j in range(input_peers):
                if j == perm[i]:
                    perm_matrix[i, j] = 1

        inv_perm_matrix = np.linalg.inv(perm_matrix)

        with open(dst, "w") as out:
            for i in range(input_peers):
                for j in range(input_peers):
                    out.write(f"{int(perm_matrix[i, j])} ")
            for i in range(input_peers):
                for j in range(input_peers):
                    out.write(f"{int(inv_perm_matrix[i, j])} ")


def main():
    """
    Run the MP-SPDZ code for the protocol Crossover KE presented in the paper "Malte Breuer, Ulrike Meyer and Susanne
    Wetzel. 2022. Privacy-Preserving Maximum Matching on General Graphs and its Application to Enable
    Privacy-Preserving Kidney Exchange. In Proceedings of the Twelveth ACM Conference on Data and Application
    Security and Privacy (CODASPY 2022)".
    This includes starting the protocol execution on all three computing peers
    as well as sending input and receiving output for all input peers. The output is printed to the terminal.
    """

    os.chdir(sys.path[0])
    if len(sys.argv) > 1:
        clients = int(sys.argv[1])
    else :
        clients = 3

    program = "crossover_ke"

    generate_shuffle_input(clients)

    # Run the protocol Crossover KE on the computing peers
    popen_first = subprocess.Popen(["../MPSPDZ/shamir-party.x", "-N", str(NUM_COMPUTING_PEERS), "-h", "localhost", "0",
                                    program], stdout=subprocess.PIPE, cwd="./protocols", universal_newlines=True)

    for i in range(1, NUM_COMPUTING_PEERS):
        subprocess.Popen(["../MPSPDZ/shamir-party.x", "-N", str(NUM_COMPUTING_PEERS), "-h", "localhost", str(i),
                          program], stdout=subprocess.DEVNULL, cwd="./protocols", universal_newlines=True)

    # Send the input of the input peers.
    popen_clients = []

    for i in range(int(clients)):
        if i == (int(clients) - 1):
            popen_clients.append(subprocess.Popen(["./external-client.x", str(i), str(NUM_COMPUTING_PEERS), "1",
                                                   str(program)], stdout=subprocess.PIPE, cwd="./MPSPDZ",
                                                  universal_newlines=True))
        else:
            popen_clients.append(subprocess.Popen(["./external-client.x", str(i), str(NUM_COMPUTING_PEERS), "0",
                                                   str(program)], stdout=subprocess.PIPE, cwd="./MPSPDZ",
                                                  universal_newlines=True))

    for line in popen_first.stdout:
        print(f"{WARNING_COLOR}{line}{END_COLOR}", end='')

    # Print the output of the input peers.
    for i in range(len(popen_clients)):
        for line in popen_clients[i].stdout:
            out = "Client "+str(i+1)+": "+line
            print(f"{OUTPUT_COLORS[i%len(OUTPUT_COLORS)]}{out}{END_COLOR}", end='')


if __name__ == "__main__":
    main()
