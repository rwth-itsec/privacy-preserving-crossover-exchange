import itertools
import os
import multiprocessing
import sys
from joblib import Parallel, delayed

FINAL_FILE_PREFIX = "ecgs_c3_"


def to_cycles(permutation, n):
    res = []
    left = []
    for i in range(n):
        left.append(i + 1)

    while len(left) > 0:
        current_cycle = []
        i = left[0]
        while i in left:
            left.remove(i)
            current_cycle.append(i)
            if permutation[i - 1] == i:
                break
            i = permutation[i - 1]
        res.append(tuple(current_cycle))
    return res


def filter_long_cycles_single(permutation, cycle_length):
    delete = False
    for cycle in permutation:
        if len(cycle) > cycle_length:
            delete = True
            break
    if delete:
        return []
    else:
        return permutation


def reformat(permutations):
    result = []
    for perm in permutations:
        if perm != None:
            result.append([list(x) for x in perm])
    return result


def generate_cycles_three(num_clients, graphs_dir):
    if os.path.exists("permutations"):
        os.remove("permutations")
        
    if not os.path.exists(graphs_dir):
        os.mkdir(graphs_dir)

    num_cores = multiprocessing.cpu_count()

    print(f"Generating base for num_clients={num_clients}")

    base = []
    for i in range(num_clients):
        base.append(i + 1)

    print(f"Generating permutations for num_clients={num_clients}")
    y = list(itertools.permutations(base))

    print(f"Formatting to cycle notation for num_clients={num_clients}")

    result = Parallel(n_jobs=num_cores)(delayed(to_cycles)(perm, num_clients) for perm in y)

    print(f"Filtering long cycles for num_clients={num_clients}")

    results = Parallel(n_jobs=num_cores)(delayed(filter_long_cycles_single)(perm, 3) for perm in result)
    result = [x for x in results if x != []]

    print(f"Number of permutations for num_clients={num_clients}: {len(result)}")

    print(f"Reformatting permutations for num_clients={num_clients}")

    result = reformat(result)

    print(f"Writing to file " + f"permutations for num_clients={num_clients}")

    with open(f"{graphs_dir}{FINAL_FILE_PREFIX}{num_clients}", 'w') as out:
        for perm in result:
            written = 0
            for step, cycle in enumerate(perm):

                for i in cycle:
                    written += 1
                    out.write(f"{i} ")
                if step < len(perm) - 1:
                    out.write(f"0 ")
                    written += 1

            while written < 2 * num_clients - 1:
                out.write(f"0 ")
                written += 1
            out.write("0 \n")


def main(input_peers=3):
    generate_cycles_three(input_peers, "Graphs/")


if __name__ == "__main__":
    os.chdir(sys.path[0])
    if len(sys.argv) > 1:
        try:
            main(int(sys.argv[1]))
        except IndexError:
            print("Please enter the number of input peers")
    else:
        main()
