import pickle
import os
import shutil
import sys

TEMP_DIR = "temp"
TEMP_FILE_PREFIX = TEMP_DIR + "/temp_c2_p"
FINAL_FILE_PREFIX = "ecgs_c2_"


# Returns list of all possible 2-cycles for the specified list of clients
def get_cycles(clients):
    cycles = []
    for i in clients:
        for j in range(i + 1, clients[-1] + 1):
            cycles.append((i, j))
    return cycles


def expand_graph(graph, possible_cycles):
    result = []
    graph = list(graph)
    for potential_cycle in possible_cycles:
        if potential_cycle[0] not in graph \
                and potential_cycle[1] not in graph \
                and potential_cycle[0] > graph[-2]:
            new_graph = graph.copy()
            new_graph += [potential_cycle[0], potential_cycle[1]]
            result.append(new_graph)
    return result


def reformat_graph(graph, num_clients):
    result = []
    for i in range(0, len(graph), 2):
        result += [graph[i], graph[i + 1], 0]
    for i in range(1, num_clients + 1):
        if i not in result:
            result += [i, 0]
    result += [0] * int(len(graph) / 2)
    return result


def write_reformatted_graphs_to_file(graphs, file):
    with open(file, "a") as outfile:
        for graph in graphs:
            outfile.write(" ".join(map(str, graph)) + "\n")


def write_graphs_to_file(graphs, file):
    with open(file, "wb") as outfile:
        pickle.dump(graphs, outfile)


def read_graphs_from_file(file):
    with open(file, "rb") as infile:
        return pickle.load(infile)


def generate_cycles_two(num_clients, graphs_dir):
    # Folder for temporary files
    os.mkdir(TEMP_DIR)

    if not os.path.exists(graphs_dir):
        os.mkdir(graphs_dir)

    destination_file = graphs_dir + FINAL_FILE_PREFIX

    clients = []
    for i in range(num_clients):
        clients.append(int(i + 1))

    possible_cycles = get_cycles(clients)

    # Graphs with 1 Cycle
    print(f"Generating Cycle 1...")
    graphs_one_cycle = [(tuple(x)) for x in possible_cycles]
    output = dict.fromkeys(graphs_one_cycle)
    write_graphs_to_file(output, TEMP_FILE_PREFIX + "1")

    # Graphs with 2 Cycles to n/2 Cycles
    for current_cycle_num in range(2, int(num_clients / 2) + 1):
        print(f"Generating Cycle {current_cycle_num}...")
        previous_graphs = read_graphs_from_file(TEMP_FILE_PREFIX + str(current_cycle_num - 1))

        current_graphs = []
        for graph in previous_graphs:
            current_graphs += expand_graph(graph, possible_cycles)

        output = dict.fromkeys([tuple(x) for x in current_graphs])
        write_graphs_to_file(output, TEMP_FILE_PREFIX + str(current_cycle_num))

    print("Finished Generating Cycles. Reformatting...")
    with open(destination_file + str(num_clients), "w") as outfile:  # remove file contents and write 0th graph
        for i in range(1, num_clients + 1):
            outfile.write(f"{i} 0 ")
        outfile.write("\n")

    for current_cycle_num in range(1, int(num_clients / 2) + 1):
        print(f"Reformatting for Cycle {current_cycle_num}")

        current_graphs = []
        graphs = read_graphs_from_file(TEMP_FILE_PREFIX + str(current_cycle_num))
        for graph in graphs:
            current_graphs.append(reformat_graph(graph, num_clients))

        write_reformatted_graphs_to_file(current_graphs, destination_file + str(num_clients))

    # Cleans up temporary files
    shutil.rmtree(TEMP_DIR)


def main(input_peers=3):
    generate_cycles_two(input_peers, "Graphs/")


if __name__ == "__main__":
    os.chdir(sys.path[0])
    if len(sys.argv) > 1:
        try:
            main(int(sys.argv[1]))
        except IndexError:
            print("Please enter the number of input peers")
    else:
        main()
