import numpy as np
import sys
import math

# imports from MPSPDZ Compiler
sys.path.append("MPSPDZ")
from Compiler.permutation import configure_waksman

# Size of the set ECGS for different numbers of parties (2-Cycles, 3-Cycles)
NUM_GRAPHS_C2 = [0, 1, 2, 4, 10, 26, 76, 232, 764, 2620, 9496, 35696, 140152, 568504, 2390480, 10349536]
NUM_GRAPHS_C3 = [0, 1, 2, 6, 18, 66, 276, 1212, 5916, 31068, 171576, 1014696]

def config_shuffle_secure(perm):
    """ Compute config for oblivious shuffling for one computing peer.

        Take mod 2 for active sec. """
    n = len(perm)

    if n & (n - 1) != 0:
        # pad permutation to power of 2
        m = 2 ** int(math.ceil(math.log(n, 2)))
        perm += list(range(n, m))
    config_bits = configure_waksman(perm)
    return config_bits


# Generates input bits for running the protocol with given number of clients for one computing peer
def generate_waksman_config(clients, computing_peer_id, max_cyc_length):
    if max_cyc_length == 2:
        perm_length = NUM_GRAPHS_C2[clients]
    elif max_cyc_length == 3:
        perm_length = NUM_GRAPHS_C3[clients]
    else:
        print("Maximum cycle length must be either 2 or 3. Aborting")
        exit(1)

    dst = f"protocols/Player-Data/Input-P{computing_peer_id}-0"

    perm = list(np.random.permutation(perm_length))
    config_bits = config_shuffle_secure(perm)

    with open(dst, "w") as out:
        for row in config_bits:
            for bit in row:
                out.write(f"{bit} ")
            out.write("\n")
