import sympy
import random

PRIME_RANGE = 40000
MAX_NUM_CLIENTS = 15


# Generates primes, each corresponding to a neighborhood constellation for a client
def generate_n_hoods(clients):
    if(clients > MAX_NUM_CLIENTS):
        print(f"Error: Not enough primes smaller than {PRIME_RANGE} for {MAX_NUM_CLIENTS} clients\n")
        exit(0)
    prime_list = list(sympy.primerange(0, PRIME_RANGE))

    result = []
    for i in range(clients):
        x = []
        for j in range(clients*clients):
            x.append(prime_list.pop(0))
        random.shuffle(x)
        result.append(x)
    return result
